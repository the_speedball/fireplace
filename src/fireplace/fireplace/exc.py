class NotEnoughCheddar(Exception):
    pass


class WageringRequirementNotMet(Exception):
    pass


class RealWalletConversion(Exception):
    pass
