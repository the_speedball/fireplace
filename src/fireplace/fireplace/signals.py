from django.dispatch import Signal, receiver

cashin_change = Signal(providing_args=["username", "amount"])
can_convert = Signal(providing_args=["username", "amount"])


@receiver(cashin_change)
def profile_handler(sender, **kwargs):
    from fireplace.models import User
    user = User.objects.get(username=kwargs['username'])
    user.profile.modify_cashin(kwargs['amount'])


@receiver(can_convert)
def wallet_convert_test(sender, **kwargs):
    from fireplace.models import User
    user = User.objects.get(username=kwargs['username'])
    return user.profile.can_convert(kwargs['amount'])
