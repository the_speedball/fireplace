import logging
from functools import wraps

from django.conf import settings

from fireplace.models import Wallet, User

logger = logging.getLogger(__name__)


def _add_bonus(owner, amount):
    logger.debug('Add bonus %s to user %s', amount, owner)
    try:
        bonus_wallet = Wallet.objects.get(owner=owner,
                                          depleted=False,
                                          currency=settings.BONUS_CURRENCY)
    except Wallet.DoesNotExist:
        logger.info('User %s no bonus wallet. Creating.', owner)
        bonus_wallet = Wallet(owner=owner, currency=settings.BONUS_CURRENCY)

    bonus_wallet.amount += amount
    bonus_wallet.save()


def login_bonus(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        output = f(*args, **kwargs)
        logger.info('Adding login bonus.')
        form = args[1]
        owner = User.objects.get(username=form.cleaned_data.get('username'))
        _add_bonus(owner, settings.BONUS_LOGIN_AMOUNT)
        return output

    return wrapped


def deposit_bonus(f):
    @wraps(f)
    def wrapped(username, amount, currency):
        output = f(username, amount, currency)
        if amount > settings.BONUS_DEPOSIT_THRESHOLD:
            logger.info('Deposit %s over threshold %s. Adding bonus.', amount,
                        settings.BONUS_DEPOSIT_THRESHOLD)
            owner = User.objects.get(username=username)
            _add_bonus(owner, settings.BONUS_DEPOSIT_AMOUNT)

        return output

    return wrapped
