import logging
from functools import partial
from random import choice

from django.conf import settings

from fireplace.engine.bonuses import deposit_bonus
from fireplace.exc import NotEnoughCheddar
from fireplace.models import Wallet, User

logger = logging.getLogger(__name__)
lotto_machine = partial(choice, [True, False])


def play(username, bet):
    owner = User.objects.get(username=username)
    wallets = Wallet.objects.filter(owner=owner, depleted=False)
    funded_wallets = [w for w in wallets if w.amount >= bet]
    if not funded_wallets:
        logger.info('Not enough funds to play for user: %s', username)
        raise NotEnoughCheddar('No wallet has enough funds.')

    try:
        wallet = [
            w
            for w in funded_wallets
            if w.currency != settings.BONUS_CURRENCY
        ][0]
    except IndexError:
        wallet = funded_wallets[0]

    won = lotto_machine()
    wallet.deposit(bet if won else -bet, bet=True)
    return won, wallet


@deposit_bonus
def deposit(username, amount, currency):

    owner = User.objects.get(username=username)
    try:
        wallet = Wallet.objects.get(
            owner=owner, currency=currency, depleted=False)
    except Wallet.DoesNotExist:
        wallet = Wallet(owner=owner, currency=currency)
        logger.info('Created wallet for user: %s', username)

    wallet.deposit(amount)
    return wallet.amount
