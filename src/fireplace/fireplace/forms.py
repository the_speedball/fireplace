from django import forms
from django.conf import settings
from django.core.validators import MinValueValidator


class PlayForm(forms.Form):
    bet = forms.IntegerField(
        validators=[MinValueValidator(1)], initial=settings.DEFAULT_BET_AMOUNT)


class DepositForm(forms.Form):
    amount = forms.IntegerField(
        validators=[MinValueValidator(1)],
        initial=settings.DEFAULT_DEPOSIT_AMOUNT)
    currency = forms.CharField(max_length=3, initial=settings.DEFAULT_CURRENCY)
