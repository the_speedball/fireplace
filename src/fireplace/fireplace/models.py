import logging
from random import randint

from django.conf import settings
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from fireplace.exc import WageringRequirementNotMet, RealWalletConversion
from fireplace.signals import can_convert, cashin_change

logger = logging.getLogger(__name__)


def set_wagering_requirement():
    """ Implements policy for wagering of wallets """
    return randint(1, 100)


class User(User):
    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        profile = Profile(user=self)
        profile.save()


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cash_in = models.IntegerField(default=0)

    def modify_cashin(self, amount):
        self.cash_in += amount
        self.save()

    def can_convert(self, wallet_cashin):
        return self.cash_in > wallet_cashin


class Wallet(models.Model):
    owner = models.ForeignKey(User)
    currency = models.CharField(
        blank=False, max_length=3, default=settings.DEFAULT_CURRENCY)
    amount = models.IntegerField(default=0)
    depleted = models.BooleanField(default=False)
    wagering_requirement = models.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(100)],
        default=set_wagering_requirement)

    def __str__(self):
        return 'Wallet: {amount} {currency}'.format(
            amount=self.amount, currency=self.currency)

    def deposit(self, amount, bet=False):
        self.amount += amount
        self.save()

        if not bet and self.currency != settings.BONUS_CURRENCY:
            cashin_change.send(sender=self.__class__,
                               username=self.owner.username,
                               amount=amount)

    def save(self, *args, **kwargs):
        if self.currency == settings.BONUS_CURRENCY and self.amount <= 0:
            logger.info('Wallet has been depleted for user: %s', self.owner)
            self.depleted = True

        logger.debug('Wallet saved, %s', self)
        super(Wallet, self).save(*args, **kwargs)

    @property
    def cashin_requirement(self):
        return (self.wagering_requirement * self.amount
                if self.currency == settings.BONUS_CURRENCY
                else 0)

    def convert(self):
        """ Convert bonus wallet into real wallet """
        if self.currency != settings.BONUS_CURRENCY:
            raise RealWalletConversion()

        _, wagering_req_met = can_convert.send(
            sender=self.__class__,
            username=self.owner.username,
            amount=self.cashin_requirement)[0]
        if not wagering_req_met:
            raise WageringRequirementNotMet()

        cashin_change.send(sender=self.__class__,
                           username=self.owner.username,
                           amount=-self.cashin_requirement)

        self.currency = settings.DEFAULT_CURRENCY
        self.save()
        return self
