from django.conf.urls import url
from fireplace import views

urlpatterns = [
    url(r'^login/$',
        views.Login.as_view(template_name='login.html'),
        name='login'),
    url(r'^register/$', views.Register.as_view(), name='register'),
    url(r'^deposit/', views.Deposit.as_view(), name='deposit'),
    url(r'^play/', views.Play.as_view(), name='play')
]
