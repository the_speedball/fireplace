from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.shortcuts import redirect
from django.views.generic.edit import FormView

from fireplace.engine.bonuses import login_bonus
from fireplace.engine.game import deposit, play
from fireplace.exc import NotEnoughCheddar
from fireplace.forms import DepositForm, PlayForm
from fireplace.models import User

# TODO: separate settings into dev/prod?


class Login(LoginView):
    @login_bonus
    def form_valid(self, form):
        return super(Login, self).form_valid(form)


class Register(FormView):
    template_name = 'registration.html'
    form_class = UserCreationForm
    success_url = '/login/'

    def form_valid(self, form):
        User.objects.create_user(
            form.cleaned_data['username'],
            '',  # no email
            form.cleaned_data['password1'])
        return super(Register, self).form_valid(form)


class Play(LoginRequiredMixin, FormView):
    login_url = '/login/'
    template_name = 'play.html'
    form_class = PlayForm
    success_url = '/play/'

    def form_valid(self, form):
        try:
            won, wallet = play(self.request.user.username,
                               form.cleaned_data['bet'])
        except NotEnoughCheddar:
            messages.warning(self.request, 'Not enough funds. Please deposit')
            return redirect('/deposit')

        if won:
            result = 'You won.'
        else:
            result = 'You lost.'

        message = ('{result} Keep playing. '
                   'You have: {amount} {currency}'.format(
                       result=result,
                       amount=wallet.amount,
                       currency=wallet.currency))

        messages.info(self.request, message)
        return super(Play, self).form_valid(form)


class Deposit(LoginRequiredMixin, FormView):
    login_url = '/login/'
    template_name = 'deposit.html'
    form_class = DepositForm
    success_url = '/play/'

    def form_valid(self, form):
        currency = form.cleaned_data['currency']
        amount = form.cleaned_data['amount']
        total = deposit(self.request.user.username, amount, currency)
        messages.success(
            self.request,
            'You successfully deposited {amount} {currency}'.format(
                amount=amount, currency=currency))
        messages.info(
            self.request,
            'Your total {currency} is {total}'.format(
                currency=currency, total=total))

        return super(Deposit, self).form_valid(form)
