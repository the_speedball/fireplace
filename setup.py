# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools import find_packages


setup(
    name='fireplace',
    version='1.1.0',
    author='Michał Klich',
    author_email='michal@michalklich.com',
    packages=find_packages('src', exclude=[
        '*.tests', '*.tests.*', 'tests.*', 'tests',
        '*.ez_setup', '*.ez_setup.*', 'ez_setup.*', 'ez_setup',
        '*.examples', '*.examples.*', 'examples.*', 'examples',
    ]),
    package_dir={
        '': 'src'
    },
    include_package_data=True,
    license='MIT',
    zip_safe=False,
    install_requires=['Django']
)
