from django.conf import settings
from django.test import TestCase

from fireplace.exc import RealWalletConversion, WageringRequirementNotMet
from fireplace.models import User, Wallet

TEST_USER_LOGIN = 'xcaliber'
TEST_USER_PASSWD = 'Xc4l1b3r'


def wallet_maker(owner, currency, amount):
    w = Wallet(owner=owner, currency=currency)
    w.deposit(amount)
    return w


class WalletConvertModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = User.objects.create_user(TEST_USER_LOGIN, '',
                                             TEST_USER_PASSWD)

    def tearDown(self):
        Wallet.objects.all().delete()
        self.owner.profile.cash_in = 0
        self.owner.profile.save()

    def test_raise_exc_convert_real_money_wallet(self):
        w = wallet_maker(self.owner, settings.DEFAULT_CURRENCY, 100)

        self.assertRaises(RealWalletConversion, w.convert)

    def test_raise_exc_wagering_not_met(self):
        w = wallet_maker(self.owner, settings.BONUS_CURRENCY, 100)

        self.assertRaises(WageringRequirementNotMet, w.convert)

    def test_convert_bonus_money(self):
        wallet = wallet_maker(self.owner, settings.BONUS_CURRENCY, 5)
        self.owner.profile.cash_in = 1000
        self.owner.profile.save()
        cashin_requirement = wallet.cashin_requirement

        wallet = wallet.convert()
        self.assertEquals(wallet.currency, settings.DEFAULT_CURRENCY)
        self.assertEquals(wallet.amount, wallet.amount)
        self.owner.profile.refresh_from_db()
        self.assertEquals(self.owner.profile.cash_in,
                          1000 - cashin_requirement)


class WalletDepositTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = User.objects.create_user(TEST_USER_LOGIN, '',
                                             TEST_USER_PASSWD)

    def tearDown(self):
        Wallet.objects.all().delete()
        self.owner.profile.cash_in = 0
        self.owner.profile.save()

    def test_deposit_bet_bonus_wallet_no_cashin_profile_change(self):
        w = Wallet(owner=self.owner, currency=settings.BONUS_CURRENCY)
        w.deposit(100, bet=True)

        self.assertEquals(self.owner.profile.cash_in, 0)

    def test_deposit_bonus_wallet_no_cashin_profile_change(self):
        w = Wallet(owner=self.owner, currency=settings.BONUS_CURRENCY)
        w.deposit(100)

        self.assertEquals(self.owner.profile.cash_in, 0)

    def test_deposit_real_wallet_cashin_profile_change(self):
        w = Wallet(owner=self.owner, currency=settings.DEFAULT_CURRENCY)
        w.deposit(100)

        self.owner.profile.refresh_from_db()
        self.assertEquals(self.owner.profile.cash_in, 100)


class WalletCashinPropertyTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = User.objects.create_user(TEST_USER_LOGIN, '',
                                             TEST_USER_PASSWD)

    def tearDown(self):
        Wallet.objects.all().delete()

    def test_real_wallet(self):
        w = wallet_maker(self.owner, settings.DEFAULT_CURRENCY, 100)

        self.assertEquals(w.cashin_requirement, 0)

    def test_bonus_wallet(self):
        w = wallet_maker(self.owner, settings.BONUS_CURRENCY, 100)

        self.assertNotEquals(w.cashin_requirement, 0)
