from django.conf import settings
from django.test import TestCase

from fireplace.engine.bonuses import _add_bonus, deposit_bonus, login_bonus
from fireplace.models import Wallet, User

TEST_USER_LOGIN = 'xcaliber'
TEST_USER_PASSWD = 'Xc4l1b3r'


class AddBonusTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = User.objects.create_user(TEST_USER_LOGIN, '',
                                             TEST_USER_PASSWD)

    def setUp(self):
        if self._testMethodName == 'test_wallet_depleted':
            self.wallet = Wallet.objects.create(
                owner=self.owner,
                depleted=True,
                currency=settings.BONUS_CURRENCY)
        if self._testMethodName == 'test_wallet_exists':
            self.wallet = Wallet.objects.create(
                owner=self.owner, amount=10, currency=settings.BONUS_CURRENCY)

        if self._testMethodName == 'test_real_wallet_exists':
            self.wallet = Wallet.objects.create(
                owner=self.owner,
                amount=10,
                currency=settings.DEFAULT_CURRENCY)

    def tearDown(self):
        Wallet.objects.all().delete()

    def test_no_wallet(self):
        _add_bonus(self.owner, 10)

        wallet = Wallet.objects.get(owner=self.owner)
        self.assertEquals(
            Wallet.objects.filter(
                owner=self.owner, depleted=False).count(), 1)
        self.assertEquals(wallet.amount, 10)

    def test_real_wallet_exists(self):
        wallet = Wallet.objects.get(owner=self.owner)
        self.assertEquals(wallet.amount, 10)
        self.assertEquals(wallet.currency, settings.DEFAULT_CURRENCY)

        _add_bonus(self.owner, 10)

        wallets = Wallet.objects.filter(owner=self.owner)
        for w in wallets:
            self.assertEquals(w.amount, 10)

    def test_wallet_depleted(self):
        _add_bonus(self.owner, 10)

        wallet = Wallet.objects.get(owner=self.owner, depleted=False)
        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 2)
        self.assertEquals(
            Wallet.objects.filter(
                owner=self.owner, depleted=False).count(), 1)
        self.assertEquals(wallet.amount, 10)

    def test_wallet_exists(self):
        _add_bonus(self.owner, 10)

        wallet = Wallet.objects.get(owner=self.owner, depleted=False)
        self.assertEquals(
            Wallet.objects.filter(
                owner=self.owner, depleted=False).count(), 1)
        self.assertEquals(wallet.amount, 20)


@login_bonus
def wrapped_login(foo, bar):
    pass


class MockForm(object):
    def __init__(self, data):
        self.cleaned_data = data


class LoginBonusDecoratorTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = User.objects.create_user(TEST_USER_LOGIN, '',
                                             TEST_USER_PASSWD)

    def test_login_bonus(self):
        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 0)

        wrapped_login(None, MockForm({'username': TEST_USER_LOGIN}))

        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 1)
        self.assertEquals(
            Wallet.objects.get(owner=self.owner).amount,
            settings.BONUS_LOGIN_AMOUNT)


@deposit_bonus
def wrapped_deposit(foo, bar, baz):
    pass


class DepositBonusDecoratorTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = User.objects.create_user(TEST_USER_LOGIN, '',
                                             TEST_USER_PASSWD)

    def tearDown(self):
        try:
            Wallet.objects.all().delete()
        except Exception:
            pass

    def test_deposit_less_than_threshold(self):
        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 0)

        wrapped_deposit(TEST_USER_LOGIN, settings.BONUS_DEPOSIT_THRESHOLD - 1,
                        settings.DEFAULT_CURRENCY)

        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 0)

    def test_deposit_equals_to_threshold(self):
        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 0)

        wrapped_deposit(TEST_USER_LOGIN, settings.BONUS_DEPOSIT_THRESHOLD,
                        settings.DEFAULT_CURRENCY)

        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 0)

    def test_deposit_more_than_threshold(self):
        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 0)

        wrapped_deposit(TEST_USER_LOGIN, settings.BONUS_DEPOSIT_THRESHOLD + 1,
                        settings.DEFAULT_CURRENCY)

        self.assertEquals(Wallet.objects.filter(owner=self.owner).count(), 1)
        self.assertEquals(
            Wallet.objects.get(owner=self.owner).amount,
            settings.BONUS_DEPOSIT_AMOUNT)
