from django.test import TestCase
from django.contrib.auth.models import User

from fireplace.models import Wallet

TEST_USER_LOGIN = 'xcaliber'
TEST_USER_PASSWD = 'Xc4l1b3r'


class HappyPathTest(TestCase):
    def test_play_spins(self):
        # register
        self.client.post('/register/', {
            'username': TEST_USER_LOGIN,
            'password1': TEST_USER_PASSWD,
            'password2': TEST_USER_PASSWD
        })
        user_profile = User.objects.get(username=TEST_USER_LOGIN)
        self.assertTrue(user_profile)

        # login
        login_response = self.client.login(
            username=TEST_USER_LOGIN, password=TEST_USER_PASSWD)
        self.assertTrue(login_response)

        # play with no cash, redirect to deposit
        play_response = self.client.post('/play/', {'bet': 10}, follow=True)
        self.assertTrue(
            'deposit.html' in [t.name for t in play_response.templates])

        # deposit
        deposit_response = self.client.post(
            '/deposit/', {'amount': 50,
                          'currency': 'EUR'}, follow=True)
        self.assertEquals(Wallet.objects.get(owner=user_profile).amount, 50)

        # play
        play_response = self.client.post('/play/', {'bet': 10})
        self.assertNotEquals(Wallet.objects.get(owner=user_profile).amount, 50)
