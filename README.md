## Fireplace

Test assignment application.
Done according to received description or to better put how I understand it.


## Run

Project uses docker to run. First it needs to be built.
In the root directory of the project run following command.

```bash
docker build -t fireplace:latest .
docker run -p 8000:8000 --name=fireplace -t fireplace:latest

```

## How to play

There are couple endpoints.

* `/register` - register new account
* `/login` - login user
* `/deposit` - deposit money to your wallet
* `/play` - burn your money

## Test & static code analysis

Testing, coverage, and style can be run using tox. It is recommended to use virtualenv.

```bash

tox
```
Will run unit tests.


```bash
tox -e coverage
```
Will generate coverage.


```bash
tox -e style
```
Will run `pycodestyle` against the source.
