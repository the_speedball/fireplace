FROM python:3.6

COPY . /srv
WORKDIR /srv

RUN pip install -r requirements.txt
RUN python src/fireplace/manage.py migrate

EXPOSE 8000
CMD python src/fireplace/manage.py runserver 0.0.0.0:8000
